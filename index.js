/*
	npm init / npm init -y
	npm install express
	npm install mongoose
*/
/*
	-This code will help us access contents of express module/package
		-A "module" is a software component or part of a program that contains one or more routines
	-It also allows us to access methods and functions to easily create a server
	-We store our express module to a variable so we could easily access its keywords, functions, and methods
*/

const express = require("express");
const mongoose = require("mongoose");
const app = express();

const port = 3001;

// Setup for allowing the server to handle data from request
// Allows your app to read json data
app.use(express.json());

// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));
// Reference: https://dev.to/griffitp12/express-s-json-and-urlencoded-explained-1m7o

// Insert codes here...
mongoose.connect("mongodb+srv://admin:admin@batch218-to-do.xqojp4d.mongodb.net/?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Syntax:
/*
	const schemaName = new mongoose.Schema({<keyvalue:pair>});
*/
// name & status
// "required" is used to specify that a field must not be empty.
// "default" is used if a field value is not supplied.



const taskSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Task name is required"]
	},
	status:{
		type: String,
		default: "pending"
	}

});

/*
[SECTION] Models
The variable/object "Task"can now used to run commands for interacting with our database
Models must be in singular form and capitalized
The first parameter of the Mongoose model method indicates the collection in where to store the data
The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
*/

	//modelName
const Task = mongoose.model("Task", taskSchema);

// --------------------------------------------------------------

// [SECTION] POST / insert
app.post("/tasks", (req, res)=>{

	console.log(req.body.name);

	// Goal: Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches, the value of result is null 
	// "err" is a shorthand naming convention for errors
	Task.findOne({name: req.body.name}, (err, result) => {

		if(result != null && result.name == req.body.name){

			// Return a message to the client/Postman
			return res.send("Duplicate task found");
		}
		// If no document was found
		else{

			// This is where we receive our field values
			let newTask = new Task ({
				name: req.body.name
			});

			// The "save" method will store the information to the database
			newTask.save((saveErr, savedTask) => {

				if(saveErr){

					/*
						// Will print any errors found in the counsole
						// saveErr is an error object that will contain details about the error
							// Errors normally come as an object data type
					*/
					return console.error(saveErr);
				}
				// No error's found
				else{

					// Return a status code of 201 - means success or OK in saving
					// Sends a message "New task creadet" upon successful saving
					return res.status(201).send("New task created");
				}
			})
		}
	})

});


app.get("/tasks", (req, res) => {

	Task.find( { name: "organize files"}, (err, result) =>{
		if(err){

		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})

});

/*
	1. Create a User schema.
		username - string
		password - string
	2. Create a User model.
	Take a screenshot of your mongoDB collection to show that the users collection is added.
*/

const userSchema = new mongoose.Schema({
	username:{
		type: String,
		required: [true, "Username is required"]
	},
	password:{
		type: String,
		required: [true, "Password is required"]
	}

});

const User = mongoose.model("User", userSchema);


/*
	3. Create a route for creating a user, the endpoint should be “/signup” and the http method to be used is ‘post’.
	4. Use findOne and conditional statement to check if there is already an existing username. If there is already, the program should send a response “Duplicate user found.” If there is no match  (else), it should successfully create a new user with password.
5. If there is no error encountered during the process (include status code 201), the program should send a response “New user registered”

*/

app.post("/signup", (request, response) => {

	console.log(request.body.username);

	User.findOne({name: request.body.username}, (err, result) => {

		if(result != null && result.username == request.body.username){

			
			return response.send("Duplicate user found.");
		}
		
		else{

		
			let newUser = new User ({
				username: request.body.username,
				password: request.body.password
			});

			
			newUser.save((saveErr, savedTask) => {

				if(saveErr){

			
					return console.error(saveErr);
				}
			
				else{

			
					return response.status(201).send("New user registered");
				}
			})
		}
	})

});


app.listen(port, () => console.log(`Server running at port ${port}`));
//app.liste(port, () => console.log("Server running at port"));




